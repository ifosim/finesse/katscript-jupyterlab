import { ICodeMirror } from '@jupyterlab/codemirror';
import {
  commands,
  elements,
  functions,
  keywords,
  operators,
} from './codemirror-katscript-types';

const booleans = ['True', 'true', 'False', 'false'];

function wordListRegexp(words: string[]): RegExp {
  return new RegExp('((' + words.join(')|(') + '))');
}

function symbolListRegexp(words: string[]): RegExp {
  return new RegExp('[' + words.join('') + ']');
}

function fullString(re: RegExp): RegExp {
  return new RegExp('^' + re.source + '$');
}

const regex = {
  command: wordListRegexp(commands),
  fullcommand: fullString(wordListRegexp(commands)),
  element: wordListRegexp(elements),
  fullelement: fullString(wordListRegexp(elements)),
  function: wordListRegexp(functions),
  fullfunction: fullString(wordListRegexp(functions)),
  keyword: wordListRegexp(keywords),
  fullkeyword: fullString(wordListRegexp(keywords)),
  operator: symbolListRegexp(operators),
  name: /[a-zA-Z_][a-zA-Z0-9._-]*/,
  number: /(([+-]?inf)|([+-]?(\d+\.\d*|\d*\.\d+|\d+)([eE]-?\d*\.?\d*)?j?([pnumkMGT])?))/,
  boolean: wordListRegexp(booleans),
  fullboolean: fullString(wordListRegexp(booleans)),
  bracket: /[()]/,
};

/*
 * Available tokens:
 *
 * keyword
 * atom
 * number
 * def
 * variable
 * variable-2
 * variable-3
 * property
 * operator
 * comment
 * string
 * string-2
 * meta
 * qualifier
 * builtin
 * bracket
 * tag
 * attribute
 * header
 * quote
 * hr
 * link
 */

export function defineKatScriptMode(codemirror: ICodeMirror): void {
  const cm = codemirror.CodeMirror;
  cm.defineMode('katscript', () => {
    function tokenBase(stream: CodeMirror.StringStream, state: any) {
      // Give each line a slight background, to differentiate from Python code.
      return 'line-background-katscript ' + tokenLex(stream, state);
    }

    function tokenLex(stream: CodeMirror.StringStream, state: any) {
      if (stream.sol()) {
        state.firstOnLine = true;
      }
      // Skip to the next non-space character
      if (stream.eatSpace()) {
        return;
      }
      // If it's a comment, skip the rest of the line
      if (/#/.test(stream.peek())) {
        stream.skipToEnd();
        return 'comment';
      }
      // Strings can be either single or double quoted, and quotes can be
      // escaped inside them
      if (/["']/.test(stream.peek())) {
        const char = stream.next();
        const re = RegExp('[^' + char + ']');
        stream.eatWhile(re);
        let cur = stream.current();
        while (cur[cur.length - 1] === '\\') {
          if (!stream.next()) {
            break;
          }
          stream.eatWhile(re);
          cur = stream.current();
        }
        stream.eat(char);
        return 'string';
      }
      let style = null;

      if (state.firstOnLine) {
        // Only check for component definitions and commands at the start of
        // the line.
        if (stream.match(regex['element']) || stream.match(regex['command'])) {
          // Some elements can be substrings of commands, and vice versa e.g.
          // "l" (element) and "link" (command). To choose the correct one, we
          // first continue grabbing the rest of the current word (if there is
          // any more), then test the entire word against element/command
          // names.
          stream.eatWhile(/\w/);
          const cur = stream.current();
          if (regex['fullelement'].test(cur)) {
            // Component definition
            style = 'variable-3';
            state.nextIsDef = true;
          } else if (regex['fullcommand'].test(cur)) {
            // Command
            style = 'builtin';
          }
        }
      }
      if (style === null) {
        if (state.nextIsDef && stream.match(regex['name'])) {
          // Component name
          style = 'def';
          state.nextIsDef = false;
        } else if (
          stream.match(regex['keyword']) ||
          stream.match(regex['boolean']) ||
          stream.match(regex['function'])
        ) {
          // Same reasoning as before, grab the rest of the word then check the
          // whole thing.
          stream.eatWhile(/\w/);
          const cur = stream.current();
          if (regex['fullkeyword'].test(cur)) {
            style = 'keyword';
          } else if (regex['fullboolean'].test(cur)) {
            style = 'keyword';
          } else if (regex['fullfunction'].test(cur)) {
            style = 'builtin';
          } else {
            // We accidentally grabbed a variable.
            style = 'variable';
          }
        } else if (stream.match(regex['number'])) {
          style = 'number';
        } else if (stream.match(regex['operator'])) {
          style = 'operator';
        } else if (stream.match(regex['bracket'])) {
          style = 'bracket';
        } else if (stream.match(regex['name'])) {
          style = 'variable';
        }
      }
      if (style !== null) {
        state.firstOnLine = false;
        return style;
      }
      stream.next();
    }

    return {
      startState: function () {
        return {
          tokenize: tokenBase,
          firstOnLine: true,
          nextIsDef: false,
        };
      },
      blankLine: function (state: any) {
        return 'line-background-katscript';
      },
      token: function (stream: CodeMirror.StringStream, state: any) {
        return state.tokenize(stream, state);
      },
    };
  });

  cm.defineMIME('text/x-katscript', 'katscript');
}
