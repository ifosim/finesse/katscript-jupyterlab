import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin,
} from '@jupyterlab/application';

import {
  NotebookPanel,
  INotebookTracker,
  Notebook,
} from '@jupyterlab/notebook';

import { ICodeMirror } from '@jupyterlab/codemirror';

import { Cell, CodeCell } from '@jupyterlab/cells';

import { setupKatScriptCodeMirror } from './codemirror-katscript-python';

const plugin: JupyterFrontEndPlugin<void> = {
  id: 'katscript-jupyter',
  autoStart: true,
  requires: [INotebookTracker, ICodeMirror],
  activate,
};

function activate(
  app: JupyterFrontEnd,
  tracker: INotebookTracker,
  codemirror: ICodeMirror
): void {
  setupKatScriptCodeMirror(codemirror);
  const style = document.createElement('style');
  style.type = 'text/css';
  style.innerHTML =
    '.katscript-highlight-background .katscript { background-color: rgba(0, 0, 0, 0.04); }';
  document.getElementsByTagName('head')[0].appendChild(style);
  tracker.widgetAdded.connect(activate_katscript);
}

function check_all(sender: NotebookPanel, mimeType: string): void {
  sender.content.widgets.forEach(cell => {
    if (cell instanceof CodeCell) {
      cell.model.mimeType = mimeType;
    }
  });
}

function check_cell(sender: Notebook, cell: Cell, mimeType: string): void {
  if (cell instanceof CodeCell) {
    cell.model.mimeType = mimeType;
  }
}

function activate_katscript(
  sender: INotebookTracker,
  panel: NotebookPanel
): void {
  panel.sessionContext.ready.then(() => {
    if (panel.model.defaultKernelLanguage === 'python') {
      check_all(panel, 'text/x-katscript-python');
      panel.content.activeCellChanged.connect((...args) => {
        check_cell(...args, 'text/x-katscript-python');
      });
      panel.content.addClass('katscript-highlight-background');
    }
  });
}

export default plugin;
