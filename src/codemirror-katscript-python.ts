import { ICodeMirror } from '@jupyterlab/codemirror';
import { defineMultiplexingMode } from './codemirror-my-multiplex';
import { defineKatScript2Mode } from './codemirror-katscript2';
import { defineKatScriptMode } from './codemirror-katscript';

export function setupKatScriptCodeMirror(codemirror: ICodeMirror): void {
  const cm = codemirror.CodeMirror;
  defineMultiplexingMode(codemirror);
  defineKatScript2Mode(codemirror);
  defineKatScriptMode(codemirror);

  cm.defineMode('katscript-python', (config: any) => {
    const pmode = cm.getMode(config, 'python');
    return cm.myMultiplexingMode(
      pmode,
      {
        open: /(?=#kat2)/,
        close: /(?=""")/, // Match string end without consuming it
        mode: cm.getMode(config, 'text/x-katscript2'),
        delimStyle: 'delim',
      },
      {
        open: /(?=#kat)/,
        close: /(?=""")/, // Match string end without consuming it
        mode: cm.getMode(config, 'text/x-katscript'),
        delimStyle: 'delim',
      }
    );
  });

  cm.defineMIME('text/x-katscript-python', 'katscript-python');

  cm.modeInfo.push({
    name: 'KatScript Python',
    mime: 'text/x-katscript-python',
    mode: 'katscript-python',
    ext: [],
  });

  cm.modeInfo.push({
    name: 'KatScript',
    mime: 'text/x-katscript',
    mode: 'katscript',
    ext: [],
  });

  cm.modeInfo.push({
    name: 'KatScript 2',
    mime: 'text/x-katscript2',
    mode: 'katscript2',
    ext: [],
  });
}
